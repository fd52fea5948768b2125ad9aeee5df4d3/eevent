<?php
    session_start();
    if($_SESSION['role'] != 'D') 
    {
    echo "ERROR IN SESSION";
    exit;
    }
        include_once(__DIR__."/../../includes/sql.config.php");
    include_once(__DIR__."/../../includes/general.config.php");
    $username =  $_SESSION['uname'];
    $name = $_SESSION['name'];
    define('USERNAME',$username);
    $TABLE_NAME = "course_req_table";
    

    
    include_once(__DIR__."/../../../includes/sql.config.php");
    include_once(__DIR__."/../../../includes/general.config.php");

    $sql = "SELECT `USER_ID`, `FIRST_NAME`, `LAST_NAME`, `MAIL_ID`, `MOBILE`, `ROLE`, `PASSWORD`, `DOB`, `DEPT` FROM `users_table` WHERE `USER_ID` LIKE '$username';";

    $db = mysqli_query($link,$sql);

    if(!$db)
          die("Failed to Load: ".mysqli_error($link));

    $row = mysqli_fetch_assoc($db);
    $date_db = date_create_from_format("Y-m-d",$row['DOB']);
    $date = date_format($date_db, "j F, Y");
?>
<html>
    <head>
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css"  media="screen,projection"/>
      <link href="../../../css/material-icons.css" rel="stylesheet">
            <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <style>
        nav div a img.logo-img {
              height: 100%;
              padding: 4px;
              margin-left: 40px;
          }
        .container {
           position: relative;
           top: 5%;
        }    
         .htitle {
          margin-top: 50px;
        }
        .login {
            padding: 20px;
            margin-bottom: 0px;
        }
        .fontsize
        {
           font-size: 1rem !important;
        }
        .fontsizetxt
        {
           font-size: 0.8rem !important;
        }
        .padbot
        {
          margin-top: 10px;
          margin-bottom: 0px;
        }
        .txtbot
        {
          text-transform: uppercase;
        }
        .title
        {
            font-size: 2em;
            color: #43a047;
            margin-bottom: 0px;
        }
        .iconsize
        {
          font-size: 30px !important;
        }
        </style>
    </head>

    <body>

        <nav>
            <div class="nav-wrapper orange">
                <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../../logo.png"> </img></a>
                </a>
                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
                <ul id="nav-mobile" class="right">
                    <li><a href="../../../index.php">Logout</a></li>
                    <li><a href="../home.php">Home</a></li>
                </ul>
            </div>
        </nav>
    
     
    <div class="container">
      <div class="row">
        <div class="col s12">
          <div class="card z-depth-2">
                        <div class="login orange flow-text white-text"> USER INFORMATION
                </div>
            <div class="card-content">

              <div class="form">
                

                <div class="row section">
                     
                     <div class="input-field col s8">
                      <i class="teal-text  material-icons prefix">account_circle</i>
                          <input id="regno" type="text" name="username" class="validate"">
                          <label class="grey-text fontsizetxt" for="regno">Enter Registration Number</label>
                     </div>

                     <div class="col s4 padbot">
                         <a class=" btn hvr-bounce-to-left pink" id="viewbtn">VIEW PROFILE</a>
                      </div>                  
                </div>  

                
                <div id="info" class="row section">
                     
                 <div class="section">    
                  <i class=" col s12 center teal-text large material-icons ">account_box</i>
                  <div id="uname" class=" col s12 txtbot center  ">NAME</div> <div class="row"></div>
                </div>

                 <div class="col s12">

                     <div class="input-field col s6 ">
                      <i class="teal-text  material-icons prefix">account_circle</i>
                          <input id="username" type="text"  name="username" class="validate"">
                          <label class="grey-text fontsizetxt" for="regno"> </label>
                        </div>

                        <div class="input-field col s6">
                          <i class="teal-text material-icons prefix">view_module</i>
                          <input id="dept" type="text"  class="validate">
                          <label class="grey-text fontsizetxt" for="password"></label>
                        </div>

                         <div class="input-field col s6">
                           <i class="teal-text  material-icons prefix">perm_identity</i>
                          <input id="fname" type="text"  class="validate">
                          <label class="grey-text fontsizetxt" for="name"></label>
                        </div>

                        <div class="input-field col s6">
                           <i class="teal-text  material-icons prefix">perm_identity</i>
                          <input id="lname" type="text" class="validate">
                          <label class="grey-text fontsizetxt" for="name"></label>
                        </div>

                        <div class="input-field col s6">
                          <i class="teal-text  material-icons prefix">email</i>
                          <input id="email" type="text" name="email"  class="validate">
                          <label class="grey-text fontsizetxt" for="email"></label>
                        </div>
                        
                        <div class="input-field col s6">
                          <i class="teal-text  material-icons prefix">dialpad</i>
                          <input id="dob" type="text" name="dob"  class="validate">
                          <label class="grey-text fontsizetxt" for="email"></label>
                        </div>

                        <div class="input-field col s6">
                          <i class="teal-text  material-icons prefix">call</i>
                          <input id="phone" type="text" class="validate">
                          <label class="grey-text fontsizetxt" for="role"></label>
                        </div>
                  
                      </div>
                  </div>


                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
               
	 <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="../../../js/jquery-3.1.0.min.js"></script>
      <script type="text/javascript" src="../../../js/materialize.min.js"></script>
      <script type="text/javascript" src="index.js" ></script>
    </body>
  </html>