$(document).ready(function(){

 $('select').material_select();
 Materialize.toast('User Profile Informations!', 1000, "rounded");

 $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 60 // Creates a dropdown of 15 years to control year
  });

 $("#info").hide();

 	   $("#viewbtn").click(function() {

			var regno = $('#regno').val();

	        $.ajax({
                type: 'POST',
                data: {
                  'regno': regno,
                },
                url: 'index.helper.php',
                dataType: 'json',
                beforeSend: function(){
                 
                },        
                success: function(final_result) 
                {
                    if(final_result['error'] == 0)
                    {
                   		Materialize.toast('Student doesnot exist!', 1000, "red rounded");
                    }
                    else if(final_result['error'] == 1)
                    {
                    	$("#info").show();
                    	document.getElementById("username").value=final_result['USER_ID'];
						document.getElementById("fname").value=final_result['FIRST_NAME'];
			          	document.getElementById("lname").value=final_result['LAST_NAME'];
			          	document.getElementById("email").value=final_result['MAIL_ID'];
			          	document.getElementById("phone").value=final_result['MOBILE'];
			          	document.getElementById("dob").value=final_result['DOB'];
			          	document.getElementById("dept").value=final_result['DEPT'];
                        $("#uname").html(final_result['FIRST_NAME']+' '+final_result['LAST_NAME']);
                    }
                    else if(final_result['error'] == 2)
                    {
                      Materialize.toast('Perimission Denied!', 1000, "red rounded");
                    }
                }
        });



 	   });





});
