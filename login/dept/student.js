$(document).ready(function() {
    $('select').material_select();

		    $("#question_number").prop("disabled",true);

    $('#editQuestionButton').click(function(){
        var q_id = $('#questionNumberInput').val();
        if(q_id !== "") {
            setBaseData();
        }else {
            window.alert("Enter A Question ID");
        }
    });

    function setBaseData() {
        $.ajax({
            type: 'POST',
            data: 'action=GET' + '&q_id=' + $('#questionNumberInput').val()+'&stdID='+$("#studentIDInput").val(),
            url: 'student.helper.php',
            dataType: 'json',
            success: function(response) {
                //$('body').append("<p>" +  JSON.stringify(response) + "</p>");
                if(response['error'] == 0) {
                    $('#main_form_div').show();
                    $('#add_question_button_div').show();
                    //GOOD
                    var question_name = $('#question_name').val(response['q_name']);
                    var question_desc = $('#question_description').val(response['q_desc']);
                    var test_case_1_input = $('#testcase1_input').val(response['tc_1_in']);
                    var test_case_1_output = $('#testcase1_output').val(response['tc_1_out']);
                    var test_case_2_input = $('#testcase2_input').val(response['tc_2_in']);
                    var test_case_2_output = $('#testcase2_output').val(response['tc_2_out']);
                    var test_case_3_input = $('#testcase3_input').val(response['tc_3_in']);
                    var test_case_3_output = $('#testcase3_output').val(response['tc_3_out']);
                    var test_case_4_input = $('#testcase4_input').val(response['tc_4_in']);
                    var test_case_4_output = $('#testcase4_output').val(response['tc_4_out']);
                    var test_case_5_input = $('#testcase5_input').val(response['tc_5_in']);
                    var test_case_5_output = $('#testcase5_output').val(response['tc_5_out']);

		    $("#question_number").prop("disabled",false);
		    $("#question_number").val(response['q_id']);
		    $("#question_number").prop("disabled",true);


                    Materialize.updateTextFields();

                }else if(response['error'] == 2) {
                    window.alert("Question ID Not Present");
                }else {
                    window.alert("ERROR:: CONTACT ADMIN" + response['error_msg']);
                }
            }
        });
    }


    var test_case_1_input = $('#testcase1_input').val("0");
    var test_case_1_output = $('#testcase1_output').val("0");
    var test_case_2_input = $('#testcase2_input').val("0");
    var test_case_2_output = $('#testcase2_output').val("0");
    var test_case_3_input = $('#testcase3_input').val("0");
    var test_case_3_output = $('#testcase3_output').val("0");
    var test_case_4_input = $('#testcase4_input').val("0");
    var test_case_4_output = $('#testcase4_output').val("0");
    var test_case_5_input = $('#testcase5_input').val("0");
    var test_case_5_output = $('#testcase5_output').val("0");


    $('#addQuestionBtn').click(function(){
	var c = window.confirm("Do you want to save changes?");
	if(!c) return;
	var question_id = $("#question_number").val();
	if(!question_id) {
		window.alert("Enter a Valid Question ID");
		return;
	}
        var question_name = $('#question_name').val();
        var question_desc = $('#question_description').val();
        var test_case_1_input = $('#testcase1_input').val();
        var test_case_1_output = $('#testcase1_output').val();
        var test_case_2_input = $('#testcase2_input').val();
        var test_case_2_output = $('#testcase2_output').val();
        var test_case_3_input = $('#testcase3_input').val();
        var test_case_3_output = $('#testcase3_output').val();
        var test_case_4_input = $('#testcase4_input').val();
        var test_case_4_output = $('#testcase4_output').val();
        var test_case_5_input = $('#testcase5_input').val();
        var test_case_5_output = $('#testcase5_output').val();

            $.ajax({
                type: 'POST',
                data: {
		    stdID: $("#studentIDInput").val(),
                    q_id: question_id,
                    question_name: (question_name),
                    question_desc: (question_desc),
                    test_case_1_input: (test_case_1_input),
                    test_case_2_input: (test_case_2_input),
                    test_case_3_input: (test_case_3_input),
                    test_case_4_input: (test_case_4_input),
                    test_case_5_input: (test_case_5_input),
                    test_case_1_output: (test_case_1_output),
                    test_case_2_output: (test_case_2_output),
                    test_case_3_output: (test_case_3_output),
                    test_case_4_output: (test_case_4_output),
                    test_case_5_output: (test_case_5_output),
                    action : "SET"
                },
                url: 'student.helper.php',
                dataType: 'json',
                success: function(phpdata) {
                    var response = parseInt(phpdata['error']);
                    if(response == 0) {
                        window.alert("QUESTION UPADTED");
                    }else if(response == 1) {
                        window.alert("QUESTION UPDATION FAILED");
                    }else{
                        window.alert("ERROR:: CONTACT ADMIN" + phpdata['error_msg']);
                    }

                    //$('body').append("<p>" +  JSON.stringify(response) + "</p>");
                }
            });

    });

});
