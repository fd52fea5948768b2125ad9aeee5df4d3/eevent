<?php
    session_start();
    if(!isset($_SESSION['uname']) || $_SESSION['role'] != 'C') {
        echo "ERROR IN SESSION"; 
        exit;
    }
    $username = $_SESSION['uname'];
    $name = $_SESSION['name'];
    $course_name = $_SESSION['course_name'];
    $course_id = $_SESSION['course_id'];
    
    include_once(__DIR__."/../../includes/sql.config.php");
    include_once(__DIR__."/../../includes/general.config.php");

    $sql = "SELECT * FROM FLAGS WHERE NAME LIKE 'MAX_QUESTION';";
    $db = mysqli_query($link,$sql);
    if(!$db)
        die("Failed to Insert: ".mysqli_error($link));
    $row = mysqli_fetch_assoc($db);
    $MAX_QUESTIONS = $row['VALUE'];

    $sql = "SELECT * FROM `Q_STRUCT_TABLE` WHERE `COURSE_ID` = '$course_id' ORDER BY `SEQ_ID`;";
    $db_ui_gen = mysqli_query($link,$sql);
    if(!$db_ui_gen)
        die("Failed to Insert: ".mysqli_error($link));


    function makeDiv($count,$session = 1,$level = 1) {
        global $course_id;
        $UNIQUE_ID = $course_id."".$count;
        $UNIQUE_ID_SESSION = "session$UNIQUE_ID";
        $UNIQUE_ID_LEVEL = "level$UNIQUE_ID";


        $html = "<div class=\"row\"> <div class='col s12 m4 question' id='$UNIQUE_ID'>Question $count</div><div class='col s12 m4 input-field'> 
                    <select id='$UNIQUE_ID_SESSION'>";
        for($i=1;$i <= 10;$i++) {
            if($i == $session)
                $html = $html."<option selected value='$i'>Session $i</option>";
            else
                $html = $html."<option value='$i'>Session $i</option>";
        }
        $html = $html."</select><label for='$UNIQUE_ID_SESSION'>Select A Session</label></div><div class='col s12 m4 input-field'><select id='$UNIQUE_ID_LEVEL'>";
        for($i=1;$i <= 3;$i++) {
            if($i == $session)
                $html = $html."<option selected value='$i'>Level $i</option>";
            else
                $html = $html."<option value='$i'>Level $i</option>";
        }
        $html = $html."</select><label for='$UNIQUE_ID_LEVEL'>Select Level</label></div> </div>";
        return $html;
    }
?>

<html>
<head>
<title> Question Structure</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../../css/materialize.min.css" type="text/css" /> 
<script src="../../js/jquery-3.1.0.min.js" type="text/javascript"></script>
<script src="question.js" type="text/javascript"></script>
</head>
<style>
        .container {
           position: relative;
           top: 5%;
        }  
    .srm-text {
            margin-left: 30px;
        }
        
        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
            margin-left: 40px;
        }
        .login {
            padding: 20px;
            text-transform: uppercase;
        }
        .text {
            font-size: 1.2em;
        }
        .title {
            margin: 50px;
            margin-bottom: 10px;
            margin-left: 0;
        }
        .btn {
            margin-top: 17px;
        }
</style>
<body>
    <nav>
            <div class="nav-wrapper orange">
                <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../logo.png"> </img></a>

                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
                <ul id="nav-mobile" class="right">
                    <li><a href="../../index.php">Logout</a></li>
                    <li><a href="home.php">Home</a></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="login flow-text orange white-text">
                        MANAGE QUESTION STRUCTURE
                    </div>
                    <div class="card-content">
                        <div class="row"><div class="col s12">
                        <?php
                        if(mysqli_num_rows($db_ui_gen) < 1) {
                            for($i=0;$i < $MAX_QUESTIONS; $i++) {
                                echo makeDiv($i+1);
                            }
                        }else {
                            $count = 1;
                            while($row = mysqli_fetch_assoc($db_ui_gen)) {
                                $session_id = $row['SESSION'];
                                $level = $row['LEVEL'];
                                echo makeDiv($count,$session_id,$level);
                                $count++;
                            }
                            for($i = $count; $i <= $MAX_QUESTIONS;$i++) {
                                echo makeDiv($i);
                            }
                        }
                        ?>
                            </div></div>
                        <div class=" row">
                            <div class="col s12 center">
                            <a class="btn-large pink " id="updateQuestion">UPDATE QUESTION STRUCTURE</a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    

    <!-- BASIC SETUP (DO NOT CHANGE) -->
        <script type="text/javascript" src="../../js/materialize.min.js"></script>
        <!-- DONT CHANGE ABOVE IT -->
</body>

  </html>  
