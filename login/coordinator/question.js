$(document).ready(function() {
    $('select').material_select();


    $("#updateQuestion").click(function(){
        var data = {};
        $('.question').each(function () {
            var id = $(this).attr('id');
            var sID = $("#session" + id).val();
            var lID = $("#level" + id).val();

            data[id] = {'id': id, 'sID': sID, 'lID': lID};
        });

        console.log(data);

        var c = window.confirm("Are you Sure to Proceed ?");
        if(!c) return;

        $.ajax({
            type: 'POST',
            data: {
                'data': data
            },
            url: 'question.manage.php',
            success: function(response) {
                window.alert("UPDATE SUCCESSFUL");
            }
        });

    });






});