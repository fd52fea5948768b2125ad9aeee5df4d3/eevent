<?php
    session_start();

    include_once(__DIR__."/../../includes/sql.config.php");
    include_once(__DIR__."/../../includes/general.config.php");

    $username = $_SESSION['uname'];
    $name = $_SESSION['name'];
    $faculty_id = $_REQUEST['id'];
    $course_name = $_SESSION['course_name'];
    $course_id = $_SESSION['course_id'];
    $TABLE_NAME = "Q_STRUCT_TABLE";

    $data = $_POST['data'];
    $sql = "REPLACE INTO `$TABLE_NAME` (`COURSE_ID`,`SEQ_ID`,`SESSION`,`LEVEL`) VALUES ";

    $flag = true;
    foreach ($data as $row) {
        $seqId = $row['id'];
        $lvlId = $row['lID'];
        $sessionId = $row['sID'];

        if($flag) {
            $sql = $sql." ('$course_id','$seqId','$sessionId','$lvlId')";
            $flag = false;
        }else {
            $sql = $sql." ,('$course_id','$seqId','$sessionId','$lvlId')";
        }
    }

    $sql = $sql.";";
    echo json_encode($data);

    echo "\n".$sql;

    $db = mysqli_query($link,$sql);
    if(!$db)
        echo mysqli_error($link);
    ?>