var UNATEMPTED = '#FA1717';
var UNACTIVATED = '#90A4AE';
var CURRENT = '#CDDC39';
var FINISHED = '#22aa22';

var colourOne = [];
var colourTwo = [];

var session = [];
var topics = [];
var sectors = [];


var course_name = "";
var totalCount = 0;

for (var i = 0; i < totalCount; i++) {
    sectors[i] = 1;
    colourTwo[i] = UNACTIVATED;
}

var canContinue = false;

function changeValue(totalCount) {
    $.ajax({
        type: 'POST',
        data: 'q=SESSION',
        url: 'course.get.php',
        dataType: 'json',
        success: function (phpdata) {
            course_name = phpdata['course_name'];
            for (var i = 0; i < totalCount; i++) {
                topics[i] = '<b>Question ' + (i + 1);
            }
            loadWidowWrapper();
        }
    });

    for (var r = 0; r < totalCount; r++) {
        sectors[r] = 1;
        colourTwo[r] = UNACTIVATED;

        session[r] = 1;
        colourOne[r] = '#ffebee';
    }
}

var wrapperCount = 0;
var courseCode = "";
function loadWidowWrapper() {
    if(wrapperCount !== 1) wrapperCount++;
    else loadWindow(courseCode);
}


$.ajax({
    type: 'POST',
    data: ('q=VALUES'),
    url: 'course.get.php',
    dataType: 'json',
    success: function (codedata) {
        totalCount = parseInt(codedata['totalCount']);
        changeValue(totalCount);


        var session_id = parseInt(codedata['id']);
        for(i = 0;i <= totalCount; i++) {
            var sequenceID = (session_id * 100000) + (11 * 1000) + 100 + (11 + i);
            var status = parseInt(codedata[sequenceID]);
            if (status === 1) {
                colourTwo[i] = CURRENT;
            } else if (status === 2) {
                colourTwo[i] = FINISHED;
            } else {
                colourTwo[i] = UNATEMPTED;
            }

            if (i === totalCount) {
                courseCode = parseInt(codedata['id']);
                loadWidowWrapper();
            }
        }
    }
});






var questionListOne = [];




//Onload


function loadWindow(id) {
    id = parseInt(id);
    console.log(id);
    var count = 0;
    for (var j = 0; j <= totalCount ; j++) {
        questionListOne[count] = "" + (id * 100000 + 11 * 1000 + 100 + (11 + j));
        count++;
    }
    console.log(count);
    var title = new RGraph.Drawing.Circle({
    id: 'graphCanvas',
    x: 450,
    y: 450,
    radius: 100,
    options: {
        fillstyle: '#FFF', //=========//Label Circle
        textAccessible: true,
        textSize: 14,
    }
});


title.draw();

    var wheelOne = new RGraph.Pie({
        id: 'graphCanvas',
        data: session,
        options: {
            linewidth: 1,
            colors: colourOne,
            tooltipsHighlight: false,
            radius: 205,
            tooltipsEvent: 'onmousemove',
            variantDonutWidth: 80,
            tooltips: topics,
            variant: 'donut',
            strokestyle: '#000', //Border Color
            bounding: false,

        }
    });
    wheelOne.draw();


    var titleText = new RGraph.Drawing.Text({
        id: 'graphCanvas',
        x: 450,
        y: 450,
        text: course_name,
        options: {
            colors: ['#000'],
            bold: true,
            valign: 'center',
            halign: 'center',
            marker: false,
            size: 18,
        }
    });

    titleText.draw();

    var wheelTwo = new RGraph.Pie({
        id: 'graphCanvas',
        data: sectors,
        options: {
            linewidth: 1.2,
            colors: colourTwo,
            tooltipsEvent: 'onmousemove',
            tooltipsHighlight: false,
            radius: 330,
            variantDonutWidth: 120,
            tooltips: questionListOne,
            variant: 'donut',
            bounding: false,
            strokestyle: '#00',
        }
    });

    wheelTwo.on('click', function (e, shape) {
        displayResult(e, shape, 1,true);
    });
    wheelTwo.draw();
}


function displayResult(e, shape, wheelID,flag) {
    if(flag) {
        var language = $("#hidden").text().toLowerCase();
        window.location = "code/"+language+"/"+ language + ".code.php?&value=" + encodeURIComponent(shape['index']);
    }
}